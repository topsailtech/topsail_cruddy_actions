# TopsailCruddyActions
Adds default actions for REST oriented controllers


## Installation
Gemfile:

```ruby
git_source(:topsail_gem){ |repo_name| "git@bitbucket.org:topsailtech/#{repo_name}.git" }
gem 'topsail_cruddy_actions', topsail_gem: 'topsail_cruddy_actions'
gem 'topsail_will_filter', topsail_gem: 'topsail_will_filter'
```

## Usage

In your controller
```ruby
include CruddyActions
```

This concern will provide `index`, `new`, `create`, `edit`, `update` and `destroy` actions.

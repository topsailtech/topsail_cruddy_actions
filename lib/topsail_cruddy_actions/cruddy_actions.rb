module CruddyActions
  extend ActiveSupport::Concern

  include Pundit::Authorization

  included do
    # to deal with https://code.google.com/p/chromium/issues/detail?id=94369 (back button retrieves latest XHR instead of page)
    before_action ->(controller){ response.headers['Vary'] = 'Accept' }, only: :index

    def new
      set_record_variables
      authorize @record
    end

    def show
      set_record_variables
      authorize @record
    end

    def edit
      set_record_variables
      authorize @record
    end

    def index
      authorize(record_class)

      @records_unpaginated = policy_scope(record_class).filter_by(params[:filter]).sorted_by(params[:sort])
      @pagy = paginate(@records_unpaginated, params[:paginate])
      @records = @pagy.items
    end

    def create
      set_record_variables

      @record.valid? ? authorize(@record) : authorize(@record, :new?)
      @record.save ? create_success : create_failure
    end

    def update
      set_record_variables
      authorize @record

      @record.update(permitted_attributes(@record)) ? update_success : update_failure
    end

    def destroy
      set_record_variables
      authorize @record

      @record.destroy
      @record.destroyed? ? destroy_success : destroy_failure
    end

    protected

    #
    #  Any of the methods below could be overwritten in subclasses
    #
    def paginate(unpaginated_records, pagination_params = nil)
      Pagination.new(
        unpaginated_records,
        page:     pagination_params&.dig(:page),
        per_page: pagination_params&.dig(:per_page),
      )
    end

    def record_name_value(record)
      record_name_value = record.respond_to?(:name) && record.name
      "#{record.model_name.human}#{" \"#{record_name_value}\"" if record_name_value.present?}"
    end

    def set_record_variables
      @record = if params[:id].present?
                  record_class.find(params[:id])
                else
                  record_class.new(permitted_attributes(record_class.new))
                end
    end

    # default behavior for "success" actions

    def create_success
      success_flash(:record_created)
      response.headers['Location'] = url_for(action: 'show', id: @record)
      render status: :created
    end

    def update_success
      success_flash(:record_updated)
    end

    def destroy_success
      success_flash(:record_destroyed)
      response.headers['TS-Deleted'] = ActionView::RecordIdentifier.dom_id(@record) # needed by cruddy_views to update UI
    end

    # default behavior for "failed" actions

    def create_failure
      render 'create_failure', status: :unprocessable_entity
    end

    def update_failure
      render 'update_failure', status: :unprocessable_entity
    end

    def destroy_failure
      render 'destroy_failure', status: :unprocessable_entity
    end
  end

  private

  # only successfull actions set this growl/flash; Unsuccessful ones need to provide that data in the view
  def success_flash(key, record: @record)
    # The scope of a translation by default is based on the location of the
    # template file.  This provides a version of translate that will look
    # for a value based on the application_controller subclass name, then fall back to the
    # application_controller scope, regardless of where the template file is.
    i18n_opts = {
      default: :"application.#{action_name}.#{key}",
      human_name: record.model_name.human,
      plural_human_name: record.model_name.human(count: 2),
      record_name: record_name_value(record)
    }

    flash.now.notice = response.headers['Content-Description'] = t(".#{key}", **i18n_opts)
  end

  def record_class
    @record_class ||= controller_path.classify.constantize
  end

  # overwrite Pundit's param authorizer to deal with missing parameter key (e.g. :new action)
  def pundit_params_for(record)
    params.fetch(Pundit::PolicyFinder.new(record).param_key, {})
  end
end

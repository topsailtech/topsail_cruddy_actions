module CruddyActions
  class Pagination
    attr_reader :page, :per_page, :count_limit

    # Do not accept :count_limit and :max_per_page from request params! Always have controller specify this!
    def initialize(collection, count_limit: 1_000, max_per_page: 100, page: 1, per_page: 20)
      @collection = collection
      @per_page = [Integer(per_page.presence || 20), max_per_page].min
      @count_limit = count_limit
      @page = Integer(page.presence || 1)
      @page = last_page if @page > last_page
    end

    def offset
      (page - 1) * per_page
    end

    def previous_page
      first_page? ? nil : page - 1
    end

    def next_page
      last_page? ? nil : page + 1
    end

    def last_page
      return Float::INFINITY if per_page.zero?

      too_many_to_count? ? Float::INFINITY : [(Float(count) / per_page).ceil, 1].max
    end

    def first_page?
      page < 2
    end

    def last_page?
      page >= last_page
    end

    def count
      @count ||= too_many_to_count? ? Float::INFINITY : @collection.count
    end

    # @returns [Integer] the start of the pagination window. 0-based index
    def from
      (page - 1) * per_page
    end

    # @returns [Integer] the end of the pagination window. 0-based index
    def to
      [page * per_page, count].min - 1
    end

    def items
      @collection.offset(offset).limit(per_page).order(@collection.primary_key.to_sym)
    end

    # e.g. [1, :gap, 7, 8, 9, 10, 11, :gap, 36]
    def series
      answer = [1]
      answer.push(:gap) if page > 4
      answer.push *(([2, page - 2].max)..([page + 2, last_page - 1].min))
      answer.push(:gap) if page < last_page - 3
      answer.push(last_page) unless too_many_to_count?
      answer
    end

    private

    # @returns [Boolean] true if row count exceeds :count_limit.
    #                    But do exact count if pagination window reached the end of the collection anyway
    def too_many_to_count?
      return @too_many_to_count unless @too_many_to_count.nil?

      end_of_current_page = page * per_page - 1
      @too_many_to_count =
        if end_of_current_page < count_limit
          @collection.offset(count_limit + 1).any?
        else
          @collection.offset(end_of_current_page + 1).any?
        end
      @too_many_to_count
    end
  end
end

$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "topsail_cruddy_actions/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "topsail_cruddy_actions"
  spec.version     = TopsailCruddyActions::VERSION
  spec.authors     = ["Nathan"]
  spec.email       = ["querky1231@gmail.com"]
  spec.homepage    = "https://www.topsailtech.com"
  spec.summary     = "Cruddy Actions for Topsail applications"
  spec.description = "Basic CRUD actions"
  spec.license     = "TOPSAIL"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "TOPSAIL-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails"
  spec.add_dependency "pundit", ">= 2.2.0"
  # Also, the calling app must include gem
  #   git@bitbucket.org:topsailtech/topsail_will_filter.git
end
